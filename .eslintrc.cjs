module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "node":true,
        "jest":true,
        "vue/setup-compiler-macros": true //报错definePorps 修复 貌似没用
    },
    "extends": [
        "eslint:recommended",
        // ts语法规则
        "plugin:@typescript-eslint/recommended",
        // vue语法规则
        "plugin:vue/vue3-essential",
        "plugin:recommended",

    ],
    "overrides": [
        {
            "env": {
                "node": true
            },
            "files": [
                ".eslintrc.{js,cjs}"
            ],
            "parserOptions": {
                "sourceType": "script"
            }
        }
    ],
    
    "parserOptions": {
        "ecmaVersion": "latest",
        // 指定解析器 默认Esprima
        "parser": "@typescript-eslint/parser",
        "sourceType": "module",
        "jsxPragma":"React",
        "ecmaFeatures":{
            "jsx":true
        }
    },
    "plugins": [
        "@typescript-eslint",
        "vue"
    ],
    rules:{
        //esl int(https://eslint.bootcss.com/docs/rules/)
        'no-var':'error', //要求使用1et或const而不是var
        'no-multiple-empty-1ines':['warn', {max:1}],//不允许多个空行
        'no-console':process.env.NODE_ENV==='production'?'error': 'off',
        'no-debugger':process.env.NODE_ENV==='production'?'error':'off',
        'no-unexpected-multiline':'error', //禁止空余的多行
        'no-useless-escape':'off', //禁止不必要的转义字符
        //typeScript(https://typescript-eslint.io/rules)
        '@typescript-eslint/no-unused-vars':'error', //禁止定义未使用的变量
        '@typescript-eslint/prefer-ts-expect-error':'error', //禁止使用@ts-ignore
        '@typescript-eslint/no-explicit-any':'off', //禁止使用any类型
        '@typescript-eslint/no-non-null-assertion':'off',
        '@typescript-eslint/no-namespace':'off',  //禁止使用自定义 TypeScripy模块和命名空间
        '@typescript-eslint/semi':'off',
        //eslint-plugin-vue(https://eslint.vuejs.org/rules/)
        'vue/multi-word-component-names':'off', //要求组件名称始终为“-”链接的单词
        'vue/script-setup-uses-vars':'error', //防止<script setup>使用的变量<template>被标记为未
        'vue/no-mutating-props':'off', //不允许组件prop的改变
        'vue/attribute-hyphenation':'off', //对模板中的自定义组件强制执行属性命名样式
    }
}