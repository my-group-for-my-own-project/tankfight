


export interface ITank{   
    position:[number,number],
    direction:string,
    isMoving:boolean,
    isShow?:boolean,
    key:string,
    shootDelay:number,
}

export interface IBullet{
    position:[number,number],
    direction:string,
    isMoving:boolean,
    isShow:boolean
    key:string
}