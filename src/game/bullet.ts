
// enum Direction {
//     ArrowUp,
//     ArrowRight,
//     ArrowDown,
//     ArrowLeft
// }

import type { IBullet, ITank} from "../types";
import { setKey } from "../utils/comm";

class Bullet{
    // 子弹移动
    async move(bulletData:IBullet){
        if(!bulletData.isMoving){
            switch(bulletData.direction){
                case 'ArrowUp':
                    if(this.cheackPositon(bulletData)){
                        bulletData.position[1] = bulletData.position[1] - 12.5 
                        bulletData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            bulletData.isMoving = false
                            clearInterval(mySetTimeOut)
                            return bulletData
                        }, 100);
                    }else{
                        bulletData.position[1] = 0 
                        bulletData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            bulletData.isMoving = false
                            bulletData.isShow = false
                            clearInterval(mySetTimeOut)
                            return bulletData
                        }, 100);
                    }
                    break
                case 'ArrowDown':
                    if(this.cheackPositon(bulletData)){
                        bulletData.position[1] = bulletData.position[1] + 12.5
                        bulletData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            bulletData.isMoving = false
                            clearInterval(mySetTimeOut)
                            return bulletData
                        }, 100);
                    }else{
                        bulletData.position[1] = bulletData.position[1]+7.5
                        bulletData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            bulletData.isMoving = false
                            bulletData.isShow = false
                            clearInterval(mySetTimeOut)
                            return bulletData
                        }, 100);
                    }
                    break
                case 'ArrowLeft':
                    if(this.cheackPositon(bulletData)){
                        bulletData.position[0] = bulletData.position[0] - 12.5
                        bulletData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            bulletData.isMoving = false
                            clearInterval(mySetTimeOut)
                            return bulletData
                        }, 100);
                    }else{
                        bulletData.position[0] = 0 
                        bulletData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            bulletData.isMoving = false
                            bulletData.isShow = false
                            clearInterval(mySetTimeOut)
                            return bulletData
                        }, 100);
                    }
                    break
                case 'ArrowRight':
                    if(this.cheackPositon(bulletData)){
                        bulletData.position[0] = bulletData.position[0] + 12.5
                        bulletData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            bulletData.isMoving = false
                            clearInterval(mySetTimeOut)
                            return bulletData
                        }, 100);
                    }else{
                        
                        bulletData.isMoving = true
                        bulletData.position[0] = bulletData.position[0]+7.5
                        // bulletData.isShow =
                        let mySetTimeOut = await setTimeout(() => {
                            bulletData.isMoving = false
                            bulletData.isShow = false
                            
                            clearInterval(mySetTimeOut)
                            return bulletData
                        }, 100);
                    }
                    break
            }
        }
        return bulletData
        
    }
    //  判断子弹是否走到边界 走到边界  没加障碍物
    cheackPositon(bullet:IBullet){
        if(bullet.direction === 'ArrowUp' || bullet.direction === 'ArrowLeft'){
            if((bullet.position[0] - 12.5) < 0 || bullet.position[1]<0){
                return false
            }
        }else{
            if((bullet.position[0] + 12.5)>387.5 || (bullet.position[1] + 12.5) > 387.5 ){
                return false
            }
        }
        return true
    }
    
    // 初始化子弹坐标
    initializePosition(position:[number,number],direction:string){
        switch(direction){
            case 'ArrowUp':
                position[0] = position[0] + 10
                position[1] =  position[1] - 5
                break
            case 'ArrowDown':
                position[0] =  position[0] + 10
                position[1] =  position[1] + 25
                break
            case 'ArrowLeft':
                position[0] =  position[0] - 5
                position[1] =  position[1] + 10
                break
            case 'ArrowRight':
                position[0] =  position[0] + 25
                position[1] =  position[1] + 10
                break    
        }
        return position
    }
    // c创建子弹
    creatBullet(bullet:{arr?:IBullet[]},tankData:ITank){
        // let _position = 
        let _bulletData = {
            // 传入的是坦克坐标 先初始化子弹坐标
            position:this.initializePosition([tankData.position[0],tankData.position[1]],tankData.direction),
            direction:tankData.direction,
            isMoving:false,
            isShow:true,
            key:setKey()

        }
        if(bullet.arr){
            bullet.arr.push(_bulletData)
        }
        else{
            bullet.arr = [_bulletData]
        }
    }
    // 删除子弹 
    deleteBullet(bulletDataArr:IBullet[]){
        bulletDataArr?.forEach((item,index)=>{
            // item.
            if(!item.isShow){
                // item.isMoving = true
                bulletDataArr?.splice(index,1)
            }
        })
    } 
    // 判断子弹是否击中目标 击中则把子弹和被击中目标isShow置为false
    isHit(bulletArr:IBullet[],enemyArr:ITank[]){
        bulletArr.forEach(val=>{
            for(let value of enemyArr){
                // console.log(value)
            // enemyArr.forEach(value=>{
                if( val.position[0] <= (value.position[0] + 25) && val.position[0] >= value.position[0] && val.position[1]<=(value.position[1]+25) && val.position[1]>=value.position[1]){
                    val.isShow = false
                    value.isShow = false
                    // console
                    break
                }
            }
        })
    }
}

export default Bullet
    