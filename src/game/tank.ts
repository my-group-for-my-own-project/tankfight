
import type { ITank } from '../types/index'
class Tank{
  
    creatTank(tank:{arr?:ITank[]},tankData:ITank)  {
        if(tank.arr){
            tank.arr.push( {
                position:[tankData.position[0],tankData.position[1]],
                direction:tankData.direction,
                isMoving:false,
                isShow:true,
                key:tankData.key,
                shootDelay:tankData.shootDelay
            })
        }else{
            tank.arr = [
                {
                    position:[tankData.position[0],tankData.position[1]],
                    direction:tankData.direction,
                    isMoving:false,
                    isShow:true,
                    key:tankData.key,
                    shootDelay:tankData.shootDelay
                }
            ]
        }
    }
    // 删除坦克
    deleteTank(tankDataArr:ITank[]){
        tankDataArr?.forEach((item,index)=>{
            // item.
            if(!item.isShow){
                // item.isMoving = true
                tankDataArr?.splice(index,1)
            }
        })
    } 
    // 坦克移动 向面朝方向移动一格 在边界上则不移动
    async move(tankData:ITank){
        if(!tankData.isMoving){
            // console.log(isMoving)
            // console.log(tankData.direction)
            switch(tankData.direction){
                case 'ArrowUp':
                    if(this.cheackPositon(tankData.position[1],tankData.direction)){
                        tankData.position[1] = tankData.position[1] - 12.5 
                        tankData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            tankData.isMoving = false
                            clearInterval(mySetTimeOut)
                            return tankData
                        }, 250);
                    }
                    return tankData
                case 'ArrowDown':
                    if(this.cheackPositon(tankData.position[1],tankData.direction)){
                        tankData.position[1] = tankData.position[1] + 12.5
                        tankData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            tankData.isMoving = false
                            clearInterval(mySetTimeOut)
                            return tankData
                        }, 250);
                    }
                    return tankData
                case 'ArrowLeft':
                    if(this.cheackPositon(tankData.position[0],tankData.direction)){
                        tankData.position[0] = tankData.position[0] - 12.5
                        tankData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            tankData.isMoving = false
                            clearInterval(mySetTimeOut)
                            return tankData
                        }, 250);
                    }
                    return tankData
                case 'ArrowRight':
                    if(this.cheackPositon(tankData.position[0],tankData.direction)){
                        tankData.position[0] = tankData.position[0] + 12.5
                        tankData.isMoving = true
                        let mySetTimeOut = await setTimeout(() => {
                            tankData.isMoving = false
                            clearInterval(mySetTimeOut)
                            return tankData
                        }, 250);
                    }
                    return tankData
            }
        }
        return tankData
        
    }
    //  判断坦克是否走到边界 走到边界  没加障碍物
    cheackPositon(position:number,direction:string){
        if(direction === 'ArrowUp' || direction === 'ArrowLeft'){
            if((position - 12.5)< 0){
                return false
            }
        }else{
            if((position+12.5)>375){
                return false
            }
        }
        return true
    }
    // 射击延迟
    async shootDelay(tankData:ITank){
        // let _test = false
        let shootDelay = tankData.shootDelay
        if(tankData.shootDelay!=0){
            tankData.shootDelay = 0
        }
        await setTimeout(()=>{
            tankData.shootDelay = shootDelay
        },shootDelay)
    }
    // 修改坦克方向
    // changeDirection(direction:string){
    //     direction = direction
    // }
    
}
export default Tank