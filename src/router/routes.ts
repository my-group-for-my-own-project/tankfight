export const routes = [
    {
        path:'/',
        name:'Home',
        component: () => import('../views/index/index.vue'),
        meta:{
            isAuth:false,
            title:'坦克大战'
        }
    }
]